import { getCurrencies } from './currencyReducer';
import { getCategories } from './categoriesReducer';
import { getSavedProducts } from './cartReducer';

const INITIALIZED_SUCCESS = 'app-reducer/INITIALIZED_SUCCESS';
const DARKEN_BACKGROUND = 'DARKEN_BACKGROUND';

const initialState = {
  initialized: false,
  darkBackground: false
  // globalError: null,
};

const appReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case INITIALIZED_SUCCESS:
      return { ...state, initialized: true };
    case DARKEN_BACKGROUND:
      return { ...state, darkBackground: action.payload };
    default:
      return state;
  }
};

export default appReducer;

export const initializedSuccess = () => {
  return {
    type: INITIALIZED_SUCCESS
  };
};

export const darkenBackground = (value) => {
  return {
    type: DARKEN_BACKGROUND,
    payload: value
  };
};

// gets saved data from local storage
export const loadState = () => {
  try {
    const serializeState = localStorage.getItem('cartProducts');
    if (serializeState === null) {
      return undefined;
    }
    return JSON.parse(serializeState);
  } catch (err) {
    return undefined;
  }
};

export const initializeApp = () => (dispatch) => {
  const categories = dispatch(getCategories());
  const currencies = dispatch(getCurrencies());
  const data = loadState();
  if (data) {
    const cartProducts = dispatch(getSavedProducts(data));
    Promise.all([currencies, categories, cartProducts]).then(() => {
      dispatch(initializedSuccess());
    });
  } else {
    Promise.all([currencies, categories]).then(() => {
      dispatch(initializedSuccess());
    });
  }
};
