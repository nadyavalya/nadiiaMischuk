import { gql } from '@apollo/client';

const CHOOSE_NEW_CATEGORY = 'CHOOSE_NEW_CATEGORY';
const SET_ALL_CATEGORIES = 'SET_ALL_CATEGORIES';

const initialState = {
  chosenCategory: '',
  allCategories: []
};

const categoriesReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case CHOOSE_NEW_CATEGORY:
      return {
        ...state,
        chosenCategory: action.payload
      };
    case SET_ALL_CATEGORIES:
      return {
        ...state,
        chosenCategory: action.payload[0].name,
        allCategories: action.payload
      };
    default:
      return state;
  }
};

export default categoriesReducer;

export const changeCategory = (category) => {
  return { type: CHOOSE_NEW_CATEGORY, payload: category };
};

export const setAllCategories = (categories) => {
  return { type: SET_ALL_CATEGORIES, payload: categories };
};

export const getCategories = () => {
  return (dispatch, getState, client) => {
    client
      .query({
        query: gql`
          query {
            categories {
              name
            }
          }
        `,
        fetchPolicy: 'no-cache'
      })
      .then((result) => {
        dispatch(setAllCategories(result.data.categories));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};
