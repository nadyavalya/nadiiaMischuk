const ADD_INTERESTED_PRODUCT = 'ADD_INTERESTED_PRODUCT';
const CHANGE_PRODUCT_ATTRIBUTE = 'CHANGE_PRODUCT_ATTRIBUTE';
const WANT_TO_BUY_PRODUCT = 'WANT_TO_BUY_PRODUCT';
const INCREASE_PRODUCT_QUANTITY = 'INCREASE_PRODUCT_QUANTITY';
const DECREASE_PRODUCT_QUANTITY = 'DECREASE_PRODUCT_QUANTITY';
const DELETE_PRODUCT = 'DELETE_PRODUCT';
const GET_SAVED_PRODUCTS = 'GET_SAVED_PRODUCTS';

const initialState = {
  productsInterestedIn: [],
  productsToBuy: [],
  totalQuantityBuy: 0,
  totalPrice: []
};

const cartReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_INTERESTED_PRODUCT: {
      // prevents of adding a duplicate to the array
      const filteredProductsInterestedIn = state.productsInterestedIn.filter((oneProduct) => {
        return oneProduct.id !== action.payload.id;
      });
      return {
        ...state,
        productsInterestedIn: [...filteredProductsInterestedIn, action.payload]
      };
    }
    case CHANGE_PRODUCT_ATTRIBUTE: {
      return {
        ...state,
        productsInterestedIn: state.productsInterestedIn.map((oneProduct) => {
          if (oneProduct.id === action.payload.productId) {
            const newAttributes = oneProduct.attributes.map((element) => {
              if (element.attributeId === action.payload.attributeId) {
                return {
                  attributeId: action.payload.attributeId,
                  type: action.payload.type,
                  attribute: action.payload.attribute,
                  attributeValue: action.payload.attributeValue
                };
              }
              return element;
            });
            return { ...oneProduct, attributes: newAttributes };
          }
          return oneProduct;
        })
      };
    }
    case WANT_TO_BUY_PRODUCT: {
      // gets all products with the received id
      const productsToBuyArray = state.productsInterestedIn.filter((oneProduct) => {
        if (oneProduct.id === action.payload) {
          return oneProduct;
        }
      });
      // array of interested products
      const newArrayProducts = state.productsInterestedIn.filter((oneProduct) => {
        if (oneProduct.id !== action.payload) {
          return oneProduct;
        }
      });
      // the array with the same product id
      const unique = state.productsToBuy.filter((oneProduct) => {
        return oneProduct.id === action.payload;
      });
      let changeQuantity = '';
      // checking if the attributes are unique and changing the product quantity
      let uniqueAttributes = unique.map((oneProduct) => {
        const size = oneProduct.attributes.length;
        let count = 0;
        for (let i = 0; i < size; i++) {
          if (
            oneProduct.attributes[i].attribute ===
              productsToBuyArray[0].attributes[i].attributeValue ||
            oneProduct.attributes[i].attribute === productsToBuyArray[0].attributes[i].attribute
          ) {
            count += 1;
          }
        }
        if (count === size) {
          changeQuantity = 'true';
          return { ...oneProduct, quantity: oneProduct.quantity + 1 };
        }
        // changeQuantity = "false";
        return oneProduct;
      });
      if (changeQuantity !== 'true' && uniqueAttributes.length !== 0) {
        uniqueAttributes = [...uniqueAttributes, productsToBuyArray[0]];
      }
      if (uniqueAttributes.length === 0) {
        uniqueAttributes[0] = productsToBuyArray[0];
      }
      const oldProducts = state.productsToBuy.filter(
        (oneProduct) => oneProduct.id !== action.payload
      );
      const allProductsToBuy = [...oldProducts, ...uniqueAttributes];
      // the array to save the total price for each currency
      const totalPriceArray = allProductsToBuy[0].prices.map((price) => {
        const key = price.currency.symbol;
        return { key, total: 0 };
      });
      // counts the total price of products for each currency
      allProductsToBuy.map((oneProduct) => {
        oneProduct.prices.map((onePrice) => {
          totalPriceArray.find((element) => {
            if (element.key === onePrice.currency.symbol) {
              element.total += onePrice.amount * oneProduct.quantity;
            }
          });
        });
      });
      // counts the total quantity of products to buy
      let quantity = 0;
      allProductsToBuy.forEach((element) => {
        quantity += element.quantity;
      });
      return {
        ...state,
        productsToBuy: allProductsToBuy,
        productsInterestedIn: newArrayProducts,
        totalQuantityBuy: quantity,
        totalPrice: totalPriceArray
      };
    }
    case INCREASE_PRODUCT_QUANTITY: {
      // the array to save new total prices in all currencies after product's quantity is changed
      const totalPriceArrayForIncrease = state.productsToBuy[0].prices.map((price) => {
        const key = price.currency.symbol;
        return { key, total: 0 };
      });
      // the array to save the products to buy after it's quantity is changed
      const arrayOfDataIncrease = {
        ...state,
        productsToBuy: state.productsToBuy.map((oneProduct) => {
          if (oneProduct.id === action.payload.id) {
            const size = oneProduct.attributes.length;
            let count = 0;
            for (let i = 0; i < size; i++) {
              if (
                oneProduct.attributes[i].attribute ===
                  action.payload.attributes[i].attributeValue ||
                oneProduct.attributes[i].attribute === action.payload.attributes[i].attribute
              ) {
                count += 1;
              }
            }
            if (count === size) {
              return { ...oneProduct, quantity: oneProduct.quantity + 1 };
            }
            return oneProduct;
          }
          return oneProduct;
        }),
        totalQuantityBuy: state.totalQuantityBuy + 1
      };
      // counts the new total price after the products quantity is changed
      arrayOfDataIncrease.productsToBuy.map((oneProduct) => {
        oneProduct.prices.map((onePrice) => {
          totalPriceArrayForIncrease.find((element) => {
            if (element.key === onePrice.currency.symbol) {
              element.total += onePrice.amount * oneProduct.quantity;
            }
          });
        });
      });
      return { ...arrayOfDataIncrease, totalPrice: totalPriceArrayForIncrease };
    }
    case DECREASE_PRODUCT_QUANTITY: {
      // the array to save new total prices in all currencies after product's quantity is changed
      const totalPriceArrayForDecrease = state.productsToBuy[0].prices.map((price) => {
        const key = price.currency.symbol;
        return { key, total: 0 };
      });
      // the array to say the products to buy after it's quantity is changed
      const newQuantity = state.productsToBuy.map((oneProduct) => {
        if (oneProduct.id === action.payload.id) {
          const size = oneProduct.attributes.length;
          let count = 0;
          for (let i = 0; i < size; i++) {
            if (
              oneProduct.attributes[i].attribute === action.payload.attributes[i].attributeValue ||
              oneProduct.attributes[i].attribute === action.payload.attributes[i].attribute
            ) {
              count += 1;
            }
          }
          if (count === size) {
            return { ...oneProduct, quantity: oneProduct.quantity - 1 };
          }
          return oneProduct;
        }
        return oneProduct;
      });
      // gets rid of the product if it's quantity is 0
      const arrayOfDataDecrease = {
        ...state,
        productsToBuy: newQuantity.filter((oneProduct) => oneProduct.quantity !== 0),
        totalQuantityBuy: state.totalQuantityBuy - 1
      };
      // counts the new total price for products in all currencies
      arrayOfDataDecrease.productsToBuy.map((oneProduct) => {
        oneProduct.prices.map((onePrice) => {
          totalPriceArrayForDecrease.find((element) => {
            if (element.key === onePrice.currency.symbol) {
              element.total += onePrice.amount * oneProduct.quantity;
            }
          });
        });
      });
      return { ...arrayOfDataDecrease, totalPrice: totalPriceArrayForDecrease };
    }
    case DELETE_PRODUCT: {
      // the array to save new total prices in all currencies after product is deleted
      const totalPriceArrayForDelete = state.productsToBuy[0].prices.map((price) => {
        const key = price.currency.symbol;
        return { key, total: 0 };
      });
      // deletes the product according to received data
      const productsToBuyAfterDelete = state.productsToBuy.filter((oneProduct) => {
        if (oneProduct.id === action.payload.id) {
          const size = oneProduct.attributes.length;
          let count = 0;
          for (let i = 0; i < size; i++) {
            if (oneProduct.attributes[i].attribute === action.payload.attributes[i].attribute) {
              count += 1;
            }
          }
          if (count === size) {
            return;
          }
          if (count !== size) {
            return oneProduct;
          }
        }
        return oneProduct;
      });
      // counts the new quantity after the product is deleted
      let newQuantityForDelete = 0;
      productsToBuyAfterDelete.map((oneProduct) => {
        newQuantityForDelete += oneProduct.quantity;
      });
      // counts the new total price after product is deleted
      productsToBuyAfterDelete.map((oneProduct) => {
        oneProduct.prices.map((onePrice) => {
          totalPriceArrayForDelete.find((element) => {
            if (element.key === onePrice.currency.symbol) {
              element.total += onePrice.amount * oneProduct.quantity;
            }
          });
        });
      });
      return {
        ...state,
        productsToBuy: productsToBuyAfterDelete,
        totalQuantityBuy: newQuantityForDelete,
        totalPrice: totalPriceArrayForDelete
      };
    }
    case GET_SAVED_PRODUCTS: {
      if (action.payload.length > 0) {
        // the array to save new total prices in all currencies after product is deleted
        const totalPriceArrayForSavedProducts = action.payload[0].prices.map((price) => {
          const key = price.currency.symbol;
          return { key, total: 0 };
        });
        // counts the new quantity after the products are gotten from local storage
        let newQuantityForSavedProducts = 0;
        action.payload.map((oneProduct) => {
          newQuantityForSavedProducts += oneProduct.quantity;
        });
        // counts the new total price after the products are gotten from local storage
        action.payload.map((oneProduct) => {
          oneProduct.prices.map((onePrice) => {
            totalPriceArrayForSavedProducts.find((element) => {
              if (element.key === onePrice.currency.symbol) {
                element.total += onePrice.amount * oneProduct.quantity;
              }
            });
          });
        });
        return {
          ...state,
          productsToBuy: action.payload,
          totalQuantityBuy: newQuantityForSavedProducts,
          totalPrice: totalPriceArrayForSavedProducts
        };
      }
      return {
        ...state,
        products: []
      };
    }
    default:
      return state;
  }
};

export default cartReducer;

export const addInterestedProduct = (product) => {
  return { type: ADD_INTERESTED_PRODUCT, payload: product };
};

export const changeAttributeToProduct = (attribute) => {
  return { type: CHANGE_PRODUCT_ATTRIBUTE, payload: attribute };
};

export const wantToBuyProduct = (id) => {
  return { type: WANT_TO_BUY_PRODUCT, payload: id };
};

export const increaseQuantity = (id, attributes) => {
  return { type: INCREASE_PRODUCT_QUANTITY, payload: { id, attributes } };
};

export const decreaseQuantity = (id, attributes) => {
  return { type: DECREASE_PRODUCT_QUANTITY, payload: { id, attributes } };
};

export const deleteProduct = (id, attributes) => {
  return { type: DELETE_PRODUCT, payload: { id, attributes } };
};

export const getSavedProducts = (products) => {
  return { type: GET_SAVED_PRODUCTS, payload: products };
};
