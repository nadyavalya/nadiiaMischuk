import { gql } from '@apollo/client';

const CHOOSE_NEW_CURRENCY = 'SEND_MESSAGE';
const SET_ALL_CURRENCIES = 'SET_ALL_CURRENCIES';

const initialState = {
  chosenCurrency: '',
  allCurrencies: []
};

const currencyReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case CHOOSE_NEW_CURRENCY:
      return {
        ...state,
        chosenCurrency: action.payload
      };
    case SET_ALL_CURRENCIES:
      return {
        ...state,
        chosenCurrency: action.payload[0].symbol,
        allCurrencies: action.payload
      };
    default:
      return state;
  }
};

export default currencyReducer;

export const changeCurrency = (currency) => {
  return { type: CHOOSE_NEW_CURRENCY, payload: currency };
};

export const setAllCurrencies = (currencies) => {
  return { type: SET_ALL_CURRENCIES, payload: currencies };
};

export const getCurrencies = () => async (dispatch, getState, client) => {
  client
    .query({
      query: gql`
        query {
          currencies {
            label
            symbol
          }
        }
      `,
      fetchPolicy: 'no-cache'
    })
    .then((result) => {
      dispatch(setAllCurrencies(result.data.currencies));
    })
    .catch(function (error) {
      console.log(error);
    });
};
