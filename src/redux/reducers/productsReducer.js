import { gql } from '@apollo/client';
import { addInterestedProduct, wantToBuyProduct } from './cartReducer';

const SET_ALL_PRODUCTS = 'SET_ALL_PRODUCTS';
const SET_ONE_PRODUCT = 'SET_ONE_PRODUCT';
const SET_PRODUCT_ID = 'SET_PRODUCT_ID';
const CLEAR_DATA_ONE_PRODUCT = 'CLEAR_DATA_ONE_PRODUCT';

const initialState = {
  allProducts: [],
  oneProductId: '',
  oneProduct: ''
};

const productsReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_ALL_PRODUCTS:
      return {
        ...state,
        allProducts: action.payload
      };
    case SET_ONE_PRODUCT:
      return {
        ...state,
        oneProduct: action.payload
      };
    case SET_PRODUCT_ID:
      return {
        ...state,
        oneProductId: action.payload
      };
    case CLEAR_DATA_ONE_PRODUCT:
      return { ...state, oneProduct: '' };
    default:
      return state;
  }
};

export default productsReducer;

export const setAllProducts = (products) => {
  return { type: SET_ALL_PRODUCTS, payload: products };
};

export const setOneProduct = (product) => {
  return { type: SET_ONE_PRODUCT, payload: product };
};

export const setOneProductId = (id) => {
  return { type: SET_PRODUCT_ID, payload: id };
};

export const clearDataOneProduct = () => {
  return { type: CLEAR_DATA_ONE_PRODUCT };
};

export const getProducts = (category) => async (dispatch, getState, client) => {
  client
    .query({
      query: gql`
    query {
      category(input: { title: "${category}" }) {
        name
        products {
          id
          name
          brand
          inStock
          gallery
          prices{
            amount
            currency{
              symbol,
              label
            }
          }
        }
      }
    }
    `,
      fetchPolicy: 'no-cache'
    })
    .then((result) => {
      dispatch(setAllProducts(result.data.category.products));
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getOneProduct = (id) => async (dispatch, getState, client) => {
  client
    .query({
      query: gql`
        query {
          product(id: "${id}" ) {
            id
            name
            brand
            gallery
            description
            attributes{
              id
              name
              type
              items{
                id 
                displayValue
                value
              }
            }
            prices {
              amount
              currency {
                label
                symbol
              }
            }
          }
        }
      `,
      fetchPolicy: 'no-cache'
    })
    .then((result) => {
      dispatch(setOneProduct(result.data.product));
      dispatch(
        addInterestedProduct({
          id: result.data.product.id,
          quantity: 1,
          pictures: result.data.product.gallery,
          prices: result.data.product.prices,
          name: result.data.product.name,
          brand: result.data.product.brand,
          attributes: result.data.product.attributes.map((oneAttribute) => {
            return {
              productId: result.data.product.id,
              attributeId: oneAttribute.id,
              type: oneAttribute.type,
              attribute: oneAttribute.items[0].value,
              attributeValue: oneAttribute.items[0].value
            };
          }),
          defaultAttributes: result.data.product.attributes
        })
      );
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getOneProductAndBuy = (id) => async (dispatch, getState, client) => {
  client
    .query({
      query: gql`
      query {
        product(id: "${id}" ) {
          id
          name
          brand
          gallery
          description
          attributes{
            id
            name
            type
            items{
              id 
              displayValue
              value
            }
          }
          prices {
            amount
            currency {
              label
              symbol
            }
          }
        }
      }
    `,
      fetchPolicy: 'no-cache'
    })
    .then((result) => {
      dispatch(setOneProduct(result.data.product));
      dispatch(
        addInterestedProduct({
          id: result.data.product.id,
          quantity: 1,
          pictures: result.data.product.gallery,
          prices: result.data.product.prices,
          name: result.data.product.name,
          brand: result.data.product.brand,
          attributes: result.data.product.attributes.map((oneAttribute) => {
            return {
              productId: result.data.product.id,
              attributeId: oneAttribute.id,
              type: oneAttribute.type,
              value: oneAttribute.value,
              attribute: oneAttribute.items[0].value,
              attributeValue: oneAttribute.items[0].value
            };
          }),
          defaultAttributes: result.data.product.attributes
        })
      );
      dispatch(wantToBuyProduct(result.data.product.id));
    })
    .catch(function (error) {
      console.log(error);
    });
};
