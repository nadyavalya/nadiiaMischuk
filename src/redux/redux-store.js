import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import currencyReducer from './reducers/currencyReducer';
import categoriesReducer from './reducers/categoriesReducer';
import appReducer from './reducers/appReducer';
import productsReducer from './reducers/productsReducer';
import cartReducer from './reducers/cartReducer';

export const client = new ApolloClient({
  uri: process.env.REACT_APP_BACKEND_URL,
  cache: new InMemoryCache()
});

const reducers = combineReducers({
  app: appReducer,
  currency: currencyReducer,
  categories: categoriesReducer,
  products: productsReducer,
  cart: cartReducer
});

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk.withExtraArgument(client)))
);

window.store = store;

export default store;
