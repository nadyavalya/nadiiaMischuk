import React from 'react';
import './Currency.scss';

class Currency extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAllCurrencies: false,
      isVisible: false
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  myRef = React.createRef();

  // closes the currency window if the click is outside of it
  handleClickOutside = (e) => {
    if (!this.myRef.current.contains(e.target)) {
      this.setState({ showAllCurrencies: false, isVisible: false });
    }
  };

  // shows or hides the list of available currencies
  showCurrencies = (value) => {
    this.setState({ showAllCurrencies: value });
    if (value === true) {
      this.setState({ isVisible: true });
    } else {
      this.setState({ isVisible: false });
    }
  };

  // changes the active currency
  chooseCurrency = (e, language) => {
    e.preventDefault();
    const { changeCurrency } = this.props;
    changeCurrency(language);
    this.showCurrencies(false);
  };

  render() {
    return (
      <div ref={this.myRef}>
        <div
          className="chosenCurrency"
          onClick={() => {
            this.state.isVisible ? this.showCurrencies(false) : this.showCurrencies(true);
          }}>
          <p>{this.props.chosenCurrency}</p>
          {this.state.showAllCurrencies ? (
            <p className="arrowDownHeader" />
          ) : (
            <p className="arrowUpHeader" />
          )}
        </div>
        {this.state.showAllCurrencies && (
          <ul className="currenciesList">
            {this.props.allCurrencies.map((el) => {
              return (
                <li
                  key={el.label}
                  onClick={(e) => {
                    this.chooseCurrency(e, el.symbol);
                  }}>
                  {el.symbol} {el.label}
                </li>
              );
            })}
          </ul>
        )}
      </div>
    );
  }
}

export default Currency;
