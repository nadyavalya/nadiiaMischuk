import React from 'react';
import classNames from 'classnames';
import './Product.scss';

class ProductAttribute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isChosen: false,
      isChosenSwatch: false
    };
  }

  // changes the attribute when it's being clicked
  handleOnClickAttribute = (id, type, value) => {
    if (type === 'text') {
      this.setState({ isChosen: id });
      this.props.getActiveAttributes(this.props.attributeId, type, id, value);
    }
    if (type === 'swatch') {
      this.setState({ isChosenSwatch: id });
      this.props.getActiveAttributes(this.props.attributeId, type, id, value);
    }
    const { changeAttributeToProduct } = this.props;
    changeAttributeToProduct({
      productId: this.props.productId,
      attributeId: this.props.attributeId,
      type: this.props.type,
      attribute: id,
      attributeValue: value
    });
  };

  componentDidMount() {
    if (this.props.type === 'text') {
      this.setState({ isChosen: this.props.productAttributes.items[0].id });
      this.props.getActiveAttributes(
        this.props.attributeId,
        this.props.type,
        this.props.productAttributes.items[0].id,
        this.props.productAttributes.items[0].id
      );
    }
    if (this.props.type === 'swatch') {
      this.setState({
        isChosenSwatch: this.props.productAttributes.items[0].id
      });
      this.props.getActiveAttributes(
        this.props.attributeId,
        this.props.type,
        this.props.productAttributes.items[0].id,
        this.props.productAttributes.items[0].value
      );
    }
    const { changeAttributeToProduct } = this.props;
    changeAttributeToProduct({
      productId: this.props.productId,
      attributeId: this.props.attributeId,
      type: this.props.type,
      attribute: this.props.productAttributes.items[0].id,
      attributeValue: this.props.productAttributes.items[0].value
    });
  }

  render() {
    return (
      <div>
        {this.props.productAttributes.name && (
          <p className="attributesTitle">{this.props.productAttributes.name.toUpperCase()}:</p>
        )}
        <div className="attributesWrapper">
          {this.props.type === 'swatch' &&
            this.props.productAttributes.items.map((item) => {
              return (
                <div
                  key={item.id}
                  className={classNames({
                    boxAttributes: this.state.isChosenSwatch !== item.id,
                    activeAttributeBoxSwatch: this.state.isChosenSwatch === item.id
                  })}
                  style={{ backgroundColor: `${item.value}` }}
                  onClick={() => {
                    this.handleOnClickAttribute(item.id, this.props.type, item.value);
                  }}
                />
              );
            })}
          {this.props.type === 'text' &&
            this.props.productAttributes.items.map((item) => {
              return (
                <div
                  key={item.id}
                  className={classNames({
                    boxAttributes: this.state.isChosen !== item.id,
                    activeAttributeBox: this.state.isChosen === item.id
                  })}
                  onClick={() => {
                    this.handleOnClickAttribute(item.id, this.props.type, item.value);
                  }}>
                  {item.value}
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default ProductAttribute;
