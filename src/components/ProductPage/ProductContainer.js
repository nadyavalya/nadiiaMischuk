import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { compose } from 'redux';
import { getOneProduct, clearDataOneProduct } from '../../redux/reducers/productsReducer';
import {
  changeAttributeToProduct,
  wantToBuyProduct,
  addInterestedProduct
} from '../../redux/reducers/cartReducer';
import Product from './Product';

class ProductContainer extends React.Component {
  render() {
    return (
      <div>
        <Product
          key={this.props.chosenCategory}
          chosenCategory={this.props.chosenCategory}
          chosenCurrency={this.props.chosenCurrency}
          oneProduct={this.props.oneProduct}
          darkBackground={this.props.darkBackground}
          interestedProduct={this.props.interestedProduct}
          history={this.props.history}
          allProducts={this.props.allProducts}
          getOneProduct={this.props.getOneProduct}
          productId={this.props.match.params.productId}
          clearDataOneProduct={this.props.clearDataOneProduct}
          changeAttributeToProduct={this.props.changeAttributeToProduct}
          wantToBuyProduct={this.props.wantToBuyProduct}
          addInterestedProduct={this.props.addInterestedProduct}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  chosenCurrency: state.currency.chosenCurrency,
  chosenCategory: state.categories.chosenCategory,
  oneProduct: state.products.oneProduct,
  darkBackground: state.app.darkBackground,
  interestedProduct: state.cart.productsInterestedIn,
  allProducts: state.products.allProducts
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    getOneProduct,
    clearDataOneProduct,
    changeAttributeToProduct,
    wantToBuyProduct,
    addInterestedProduct
  })
)(ProductContainer);
