import React from 'react';
import classNames from 'classnames';
import parse from 'html-react-parser';
import './Product.scss';
import ProductAttribute from './ProductAttribute';

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bigImage: '',
      updateImage: true,
      inStock: true,
      chosenAttributes: []
    };
  }

  componentDidMount() {
    const { getOneProduct } = this.props;
    const { productId } = this.props;
    getOneProduct(productId);
    const { allProducts } = this.props;
    //checking whether the product is in stock
    allProducts.map((oneProduct) => {
      if (oneProduct.id === productId) {
        this.setState(() => ({
          inStock: oneProduct.inStock
        }));
      }
    });
  }

  componentWillUnmount() {
    const { clearDataOneProduct } = this.props;
    clearDataOneProduct();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.updateImage) {
      if (nextProps.oneProduct.gallery) {
        if (prevState.bigImage !== nextProps.oneProduct.gallery[0]) {
          return {
            bigImage: nextProps.oneProduct.gallery[0]
          };
        }
      }
    }
    return null;
  }

  // changes the big photo
  handleOnClickPhoto = (image) => {
    this.setState(() => ({
      bigImage: image,
      updateImage: false
    }));
  };

  // adds the product to cart
  handleOnClickAddToCard = () => {
    const { wantToBuyProduct } = this.props;
    wantToBuyProduct(this.props.productId);
    const { addInterestedProduct } = this.props;
    let value = '';
    let valueTwo = '';
    addInterestedProduct({
      id: this.props.oneProduct.id,
      quantity: 1,
      pictures: this.props.oneProduct.gallery,
      prices: this.props.oneProduct.prices,
      name: this.props.oneProduct.name,
      brand: this.props.oneProduct.brand,
      attributes: this.props.oneProduct.attributes.map((oneAttribute) => {
        this.state.chosenAttributes.forEach((el) => {
          if (el.attributeId === oneAttribute.id) {
            value = el.value;
            valueTwo = el.attributeValue;
            return;
          }
        });
        return {
          productId: this.props.oneProduct.id,
          attributeId: oneAttribute.id,
          type: oneAttribute.type,
          attribute: value,
          attributeValue: valueTwo
        };
      }),
      defaultAttributes: this.props.oneProduct.attributes
    });
  };

  //gets the chosen attributes
  getActiveAttributes = (attributeId, type, value, attributeValue) => {
    let changed = '';
    this.setState(
      (prevState) => ({
        chosenAttributes: prevState.chosenAttributes.map((obj) => {
          if (obj.attributeId === attributeId) {
            changed = 'true';
            return { ...obj, value: value, attributeValue: attributeValue };
          }
          return obj;
        })
      }),
      () => this.addNewAttribute(changed, attributeId, type, value, attributeValue)
    );
  };

  //adds the new attribute if it wasn't added before
  addNewAttribute = (changed, attributeId, type, value, attributeValue) => {
    if (changed !== 'true') {
      this.setState((prevState) => ({
        chosenAttributes: [
          ...prevState.chosenAttributes,
          { attributeId, type, value, attributeValue }
        ]
      }));
    }
  };

  render() {
    return (
      <main
        className={classNames({
          darkBackground: this.props.darkBackground
        })}>
        <div
          className={classNames({
            wrapper: true,
            oneProductWrapper: true
          })}>
          <div className="imagesContainer">
            <div className="smallImagesContainer">
              {this.props.oneProduct.gallery &&
                this.props.oneProduct.gallery.slice(0, 5).map((onePhoto) => {
                  return (
                    <div
                      key={onePhoto}
                      className="smallImage"
                      onClick={() => {
                        this.handleOnClickPhoto(onePhoto);
                      }}>
                      <img src={onePhoto} />
                    </div>
                  );
                })}
            </div>
            <div
              className={classNames({
                ['bigImage']: true,
                ['bigImageOutOfStock']: !this.state.inStock
              })}>
              {this.props.oneProduct.gallery && <img src={this.state.bigImage} />}
              {!this.state.inStock && <p>OUT OF STOCK</p>}
            </div>
          </div>
          <div className="productInfoContainer">
            <h4 className="productBrand">{this.props.oneProduct.brand}</h4>
            <h4 className="productName">{this.props.oneProduct.name}</h4>

            {this.props.oneProduct.attributes &&
              this.props.oneProduct.attributes.map((attribute) => {
                return (
                  <ProductAttribute
                    key={attribute.id}
                    attributeId={attribute.id}
                    productId={this.props.oneProduct.id}
                    productAttributes={attribute}
                    type={attribute.type}
                    interestedProduct={this.props.interestedProduct}
                    changeAttributeToProduct={this.props.changeAttributeToProduct}
                    getActiveAttributes={this.getActiveAttributes}
                  />
                );
              })}
            <p className="productPrice">PRICE:</p>
            {this.props.oneProduct.prices &&
              this.props.oneProduct.prices.map((price) => {
                if (price.currency.symbol === this.props.chosenCurrency) {
                  return (
                    <p key={price.amount} className="productPriceAmount">
                      {price.currency.symbol}
                      {price.amount}
                    </p>
                  );
                }
              })}
            <button
              className="addToCartButton"
              disabled={!this.state.inStock}
              onClick={() => {
                this.handleOnClickAddToCard();
              }}>
              ADD TO CART
            </button>
            {this.props.oneProduct.description && (
              <div className="productInfoDetails"> {parse(this.props.oneProduct.description)}</div>
            )}
          </div>
        </div>
      </main>
    );
  }
}

export default Product;
