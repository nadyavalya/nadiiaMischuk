import React from 'react';
import classNames from 'classnames';
import './ProductsListPage.scss';
import ProductCard from './ProductCard/ProductCard';

class ProductsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { getProducts } = this.props;
    const { changeCategory } = this.props;
    const { categoryId } = this.props;
    changeCategory(categoryId);
    getProducts(categoryId);
  }

  render() {
    return (
      <main
        className={classNames({
          darkBackground: this.props.darkBackground
        })}>
        <div className={classNames('wrapper')}>
          <h2>
            {this.props.categoryId.charAt(0).toUpperCase()}
            {this.props.categoryId.slice(1)}
          </h2>
          <div className="productsListWrapper">
            {this.props.allProducts.map((product) => {
              let currencyAmount;
              let currencySymbol;
              product.prices.map((oneCurrency) => {
                if (this.props.chosenCurrency === oneCurrency.currency.symbol) {
                  currencyAmount = oneCurrency.amount;
                  currencySymbol = oneCurrency.currency.symbol;
                }
              });
              return (
                <ProductCard
                  key={product.id}
                  id={product.id}
                  name={product.name}
                  brand={product.brand}
                  inStock={product.inStock}
                  image={product.gallery[0]}
                  currencySymbol={currencySymbol}
                  currencyAmount={currencyAmount}
                  chosenCategory={this.props.chosenCategory}
                  setOneProductId={this.props.setOneProductId}
                  getOneProductAndBuy={this.props.getOneProductAndBuy}
                  clearDataOneProduct={this.props.clearDataOneProduct}
                />
              );
            })}
          </div>
        </div>
      </main>
    );
  }
}

export default ProductsList;
