import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { compose } from 'redux';
import ProductsList from './ProductsList';
import {
  getProducts,
  setOneProductId,
  getOneProductAndBuy
} from '../../redux/reducers/productsReducer';
import { changeCategory } from '../../redux/reducers/categoriesReducer';
import { clearDataOneProduct } from '../../redux/reducers/productsReducer';

class ProductsListContainer extends React.Component {
  render() {
    return (
      <div>
        <ProductsList
          key={this.props.chosenCategory}
          chosenCategory={this.props.chosenCategory}
          chosenCurrency={this.props.chosenCurrency}
          allProducts={this.props.allProducts}
          categoryId={this.props.match.params.categoryId}
          darkBackground={this.props.darkBackground}
          getProducts={this.props.getProducts}
          setOneProductId={this.props.setOneProductId}
          getOneProductAndBuy={this.props.getOneProductAndBuy}
          changeCategory={this.props.changeCategory}
          clearDataOneProduct={this.props.clearDataOneProduct}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  allProducts: state.products.allProducts,
  chosenCurrency: state.currency.chosenCurrency,
  chosenCategory: state.categories.chosenCategory,
  darkBackground: state.app.darkBackground
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    getProducts,
    setOneProductId,
    getOneProductAndBuy,
    changeCategory,
    clearDataOneProduct
  })
)(ProductsListContainer);
