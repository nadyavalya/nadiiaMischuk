import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import '../ProductsListPage.scss';

class ProductCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHovering: false
    };
  }

  // shows the cart icon when cursor is on the product card
  handleMouseOver = () => {
    this.setState(() => ({
      isHovering: true
    }));
  };

  // hides the cart icon when cursor is out of product card
  handleMouseLeave = () => {
    this.setState(() => ({
      isHovering: false
    }));
  };

  // sets the id of chosen product
  handleOnClick = (id) => {
    const { setOneProductId } = this.props;
    setOneProductId(id);
  };

  // adds the product to the cart by clicking on small cart icon on the product card
  handleOnClickQuickAdd = (e, productId) => {
    e.preventDefault();
    const { getOneProductAndBuy } = this.props;
    getOneProductAndBuy(productId);
  };

  componentWillUnmount() {
    const { clearDataOneProduct } = this.props;
    clearDataOneProduct();
  }

  render() {
    return (
      <div
        className={classNames({
          productCardWrapper: true,
          outOfStock: this.props.inStock === false
        })}
        onMouseOver={this.handleMouseOver}
        onMouseLeave={this.handleMouseLeave}
        onClick={() => {
          const { id } = this.props;
          this.handleOnClick(id);
        }}>
        <Link to={`/product/${this.props.id}`}>
          <div className="imageWrapper">
            <img src={this.props.image} />
            {!this.props.inStock && <p className="outOfStockLabel">OUT OF STOCK</p>}
            {this.state.isHovering && this.props.inStock && (
              <div
                className="cartLogoHover"
                onClick={(e) => {
                  this.handleOnClickQuickAdd(e, this.props.id);
                }}
              />
            )}
          </div>
          <div className="productCardInfoWrapper">
            <div className="productCardInfo">
              <p>{this.props.brand}</p>
              <p>{this.props.name}</p>
            </div>
            <p>
              {this.props.currencySymbol}
              {this.props.currencyAmount}
            </p>
          </div>
        </Link>
      </div>
    );
  }
}

export default ProductCard;
