import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';
import './Header.scss';
import Currency from '../Currency/Currency';
import CartHeader from '../Cart/CartWIndow/CartHeader';

class Header extends React.Component {
  //changes the category by click
  handleOnClick = (category) => {
    const { changeCategory } = this.props;
    changeCategory(category);
  };

  render() {
    return (
      <div className={classNames('wrapper', 'headerWrapper')}>
        <nav>
          <ul className="categoriesHeader">
            {this.props.allCategories.map((category) => {
              return (
                <NavLink to={`/category/${category.name}`} key={category.name}>
                  <li
                    key={category.name}
                    className={classNames({
                      ['categoriesList']: this.props.chosenCategory !== category.name,
                      ['activeCategory']: this.props.chosenCategory === category.name
                    })}
                    onClick={() => {
                      this.handleOnClick(category.name);
                    }}>
                    {category.name.toUpperCase()}
                  </li>
                </NavLink>
              );
            })}
          </ul>
        </nav>
        <div className="logo"></div>
        <div className="currencyCartWrapper">
          <Currency
            chosenCurrency={this.props.chosenCurrency}
            allCurrencies={this.props.allCurrencies}
            changeCurrency={this.props.changeCurrency}
          />
          <CartHeader
            productsToBuy={this.props.productsToBuy}
            chosenCurrency={this.props.chosenCurrency}
            totalQuantityBuy={this.props.totalQuantityBuy}
            totalPrice={this.props.totalPrice}
            chosenCategory={this.props.chosenCategory}
            increaseQuantity={this.props.increaseQuantity}
            decreaseQuantity={this.props.decreaseQuantity}
            darkenBackground={this.props.darkenBackground}
          />
        </div>
      </div>
    );
  }
}

export default Header;
