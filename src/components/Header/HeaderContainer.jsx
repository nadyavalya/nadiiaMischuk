import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import './Header.scss';
import Header from './Header';
import { changeCurrency } from '../../redux/reducers/currencyReducer';
import { changeCategory } from '../../redux//reducers/categoriesReducer';
import { increaseQuantity, decreaseQuantity } from '../../redux/reducers/cartReducer';
import { darkenBackground } from '../../redux/reducers/appReducer';

class HeaderContainer extends React.Component {
  render() {
    return (
      <>
        <Header
          chosenCurrency={this.props.currency}
          allCurrencies={this.props.allCurrencies}
          allCategories={this.props.allCategories}
          productsToBuy={this.props.productsToBuy}
          chosenCategory={this.props.chosenCategory}
          totalQuantityBuy={this.props.totalQuantityBuy}
          totalPrice={this.props.totalPrice}
          changeCurrency={this.props.changeCurrency}
          changeCategory={this.props.changeCategory}
          increaseQuantity={this.props.increaseQuantity}
          decreaseQuantity={this.props.decreaseQuantity}
          darkenBackground={this.props.darkenBackground}
        />
      </>
    );
  }
}

let mapStateToProps = (state) => ({
  currency: state.currency.chosenCurrency,
  allCurrencies: state.currency.allCurrencies,
  productsToBuy: state.cart.productsToBuy,
  totalQuantityBuy: state.cart.totalQuantityBuy,
  totalPrice: state.cart.totalPrice,
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    changeCurrency,
    changeCategory,
    increaseQuantity,
    decreaseQuantity,
    darkenBackground
  })
)(HeaderContainer);
