import React from 'react';
import '../Cart.scss';
import CartOneAttribute from './CartOneAttribute';

class CartOneProductOverlay extends React.Component {
  // increases the product quantity
  increaseProductQuantity = (id, attributes) => {
    const { increaseQuantity } = this.props;
    increaseQuantity(id, attributes);
  };

  // decreases the product quantity
  decreaseProductQuantity = (id, attributes) => {
    const { decreaseQuantity } = this.props;
    decreaseQuantity(id, attributes);
  };

  render() {
    return (
      <div className="cartOverlayOneProductContainer">
        <div className="cartOverlayTwoItemsWrapper">
          <div className="cartOverlayProductWrapper">
            <div className="cartOverlayProductInfo">
              <p>{this.props.oneProduct.brand}</p>
              <p>{this.props.oneProduct.name}</p>
              {this.props.oneProduct.prices.map((oneCurrency) => {
                if (this.props.chosenCurrency === oneCurrency.currency.symbol) {
                  return (
                    <p key={oneCurrency.currency.symbol}>
                      {oneCurrency.currency.symbol}
                      {oneCurrency.amount}
                    </p>
                  );
                }
              })}
            </div>
            <div className="cartOverlayAttributes">
              {this.props.defaultAttributes.map((oneAttribute) => {
                return (
                  <CartOneAttribute
                    key={oneAttribute.id}
                    oneAttribute={oneAttribute}
                    chosenAttributes={this.props.oneProduct.attributes}
                  />
                );
              })}
            </div>
          </div>
          <div className="cartOverlayCounter">
            <div
              onClick={() =>
                this.increaseProductQuantity(
                  this.props.oneProduct.id,
                  this.props.oneProduct.attributes
                )
              }>
              +
            </div>
            <p>{this.props.oneProduct.quantity}</p>
            <div
              onClick={() =>
                this.decreaseProductQuantity(
                  this.props.oneProduct.id,
                  this.props.oneProduct.attributes
                )
              }>
              -
            </div>
          </div>
        </div>
        <div className="cartOverlayImageWrapper">
          <img src={this.props.oneProduct.pictures[0]} />
        </div>
      </div>
    );
  }
}

export default CartOneProductOverlay;
