import React from 'react';
import { Link } from 'react-router-dom';
import '../Cart.scss';
import CartOneProductOverlay from './CartOneProductOverlay';

class CartHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCartOverlay: false,
      isVisible: false,
      darkenBackground: false
    };
    this.count = 0;
  }

  componentDidMount() {
    this.saveState(this.props.productsToBuy);
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.productsToBuy !== this.props.productsToBuy) {
      this.saveState(this.props.productsToBuy);
      if (this.props.totalQuantityBuy === 0) {
        const { darkenBackground } = this.props;
        darkenBackground(false);
      }
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  myRef = React.createRef();

  // closes the cart overlay if there is a click outside of it
  handleClickOutside = (e) => {
    if (!this.myRef.current.contains(e.target)) {
      this.setState({ showCartOverlay: false, isVisible: false });
      const { darkenBackground } = this.props;
      darkenBackground(false);
    }
  };

  // shows or hides the cart overlay and darkens the background
  showCartOverlay = (value) => {
    this.setState({ showCartOverlay: value });
    const { darkenBackground } = this.props;
    if (value === true) {
      this.setState({ isVisible: true });
      if (this.props.totalQuantityBuy > 0) {
        darkenBackground(true);
      }
    } else {
      this.setState({ isVisible: false });
      darkenBackground(false);
    }
  };

  // save the product to buy to the local storage
  saveState = (state) => {
    try {
      const serializedState = JSON.stringify(state);
      localStorage.setItem('cartProducts', serializedState);
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <div ref={this.myRef}>
        <div
          className="cartIconWrapper"
          onClick={() => {
            this.state.isVisible ? this.showCartOverlay(false) : this.showCartOverlay(true);
          }}>
          <div />
          {this.props.totalQuantityBuy > 0 ? (
            <p className="cartIconQuantity">{this.props.totalQuantityBuy}</p>
          ) : (
            ''
          )}
        </div>
        {this.props.totalQuantityBuy > 0 && this.state.showCartOverlay && (
          <div className="cartOverlayWrapper">
            <div className="cartOverlayTitle">
              <p>My bag,</p>
              <p>{this.props.totalQuantityBuy} items</p>
            </div>
            <div>
              {this.props.productsToBuy.map((oneProduct) => {
                this.count++;
                return (
                  <CartOneProductOverlay
                    key={`${oneProduct.id}${this.count}`}
                    oneProduct={oneProduct}
                    chosenCurrency={this.props.chosenCurrency}
                    totalQuantity={this.props.totalQuantityBuy}
                    defaultAttributes={oneProduct.defaultAttributes}
                    increaseQuantity={this.props.increaseQuantity}
                    decreaseQuantity={this.props.decreaseQuantity}
                  />
                );
              })}
            </div>
            <div className="cartOverlayTotalPrice">
              <p>Total</p>
              <p>
                {this.props.chosenCurrency}
                {this.props.totalPrice.find((element) => {
                  if (element.key === this.props.chosenCurrency) {
                    return (this.totalPrice = element.total);
                  }
                }) && Math.round(this.totalPrice * 100) / 100}
              </p>
            </div>
            <div className="cartOverlayButtonsWrapper" onClick={() => this.showCartOverlay(false)}>
              <Link to="/productsCart" className="cartOverlayButton">
                VIEW BAG
              </Link>
              <Link to={`/category/${this.props.chosenCategory}`} className="cartOverlayButton">
                CHECK OUT
              </Link>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default CartHeader;
