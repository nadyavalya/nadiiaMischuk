import React from 'react';
import classNames from 'classnames';
import '../Cart.scss';

class CartPageOneAttribute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chosenAttribute: ''
    };
  }

  componentDidMount() {
    this.props.oneAttribute.items.forEach((item) => {
      this.props.chosenAttributes.forEach((oneAttributeChosen) => {
        if (
          oneAttributeChosen.attributeValue === item.value &&
          oneAttributeChosen.attributeId === this.props.oneAttribute.id
        ) {
          this.setState({
            chosenAttribute: oneAttributeChosen.attributeValue
          });
        }
      });
    });
  }

  render() {
    return (
      <div className="cartPageOneAttributeWrapper">
        <p>{this.props.oneAttribute.name.toUpperCase()}:</p>
        <div className="cartOverlayAttributesContainer">
          {this.props.oneAttribute.items.map((item) => {
            if (this.props.oneAttribute.type === 'text') {
              return (
                <div
                  key={item.id}
                  className={classNames({
                    ['cartPageChosenAttribute']: this.state.chosenAttribute === item.value,
                    ['cartPageNotChosenAttribute']: this.state.chosenAttribute !== item.value
                  })}>
                  {item.value}
                </div>
              );
            } else {
              return (
                <div
                  key={item.id}
                  style={{ backgroundColor: `${item.value}` }}
                  className={classNames({
                    ['cartPageChosenAttributeColor']: this.state.chosenAttribute === item.value,
                    ['cartPageNotChosenAttributeColor']: this.state.chosenAttribute !== item.value
                  })}></div>
              );
            }
          })}
        </div>
      </div>
    );
  }
}

export default CartPageOneAttribute;
