import React from 'react';
import classNames from 'classnames';
import '../Cart.scss';
import CartPageOneProduct from './CartPageOneProduct';

class CartPage extends React.Component {
  constructor(props) {
    super(props);
    this.count = 0;
  }

  render() {
    return (
      <main
        className={classNames({
          darkBackground: this.props.darkBackground
        })}>
        <div className={classNames('wrapper', 'cartPageWrapper')}>
          <h4 className="cartPageTitle">CART</h4>
          {this.props.productsToBuy.map((oneProduct) => {
            this.count++;
            return (
              <CartPageOneProduct
                key={`${oneProduct.id}${this.count}`}
                id={oneProduct.id}
                oneProduct={oneProduct}
                chosenCurrency={this.props.chosenCurrency}
                history={this.props.history}
                increaseQuantity={this.props.increaseQuantity}
                decreaseQuantity={this.props.decreaseQuantity}
                deleteProduct={this.props.deleteProduct}
              />
            );
          })}

          <div className="cartPageTotalPrice">
            <p>
              Tax: <span>{this.props.chosenCurrency}15</span>
            </p>
            <p>
              Qty:<span>{this.props.totalQuantityBuy}</span>
            </p>
            <p>
              Total:
              <span>
                {this.props.chosenCurrency}
                {this.props.totalPrice.find((element) => {
                  if (element.key === this.props.chosenCurrency) {
                    return (this.totalPrice = element.total);
                  }
                }) && Math.round(this.totalPrice * 100) / 100}
              </span>
            </p>
          </div>
          <button
            onClick={() => {
              this.props.history.push('/buyProducts');
            }}
            className={classNames('cartOverlayButton', 'cartPageButton')}>
            ORDER
          </button>
        </div>
      </main>
    );
  }
}

export default CartPage;
