import React from 'react';
import '../Cart.scss';
import CartPageOneAttribute from './CartPageOneAttribute';

class CartPageOneProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showImage: props.oneProduct.pictures[0],
      imagesQuantity: props.oneProduct.pictures.length,
      activeImage: 0
    };
  }

  // increases the product quantity
  increaseProductQuantity = (e, id, attributes) => {
    e.preventDefault();
    const { increaseQuantity } = this.props;
    increaseQuantity(id, attributes);
  };

  // decreases the product quantity
  decreaseProductQuantity = (e, id, attributes) => {
    e.preventDefault();
    const { decreaseQuantity } = this.props;
    decreaseQuantity(id, attributes);
  };

  // shows all the images of a product
  handleOnClickImages = (e, value) => {
    e.preventDefault();
    if (value === 'increase') {
      const counter = this.state.activeImage + 1;
      if (counter < this.state.imagesQuantity) {
        this.setState(() => ({
          showImage: this.props.oneProduct.pictures[counter],
          activeImage: counter
        }));
      }
    }
    if (value === 'decrease') {
      const counter = this.state.activeImage - 1;
      if (counter >= 0) {
        this.setState(() => ({
          showImage: this.props.oneProduct.pictures[counter],
          activeImage: counter
        }));
      }
    }
  };

  render() {
    return (
      <div className="cartPageOneProductWrapper">
        <div className="cartPageInfoAttributesWrapper">
          <div className="cartPageOneProductInfo">
            <p>{this.props.oneProduct.brand}</p>
            <p>{this.props.oneProduct.name}</p>
            {this.props.oneProduct.prices.map((oneCurrency) => {
              if (this.props.chosenCurrency === oneCurrency.currency.symbol) {
                return (
                  <p key={oneCurrency.currency.symbol}>
                    {oneCurrency.currency.symbol}
                    {oneCurrency.amount}
                  </p>
                );
              }
            })}
          </div>
          <div className="cartPageAttributesWrapper">
            {this.props.oneProduct.defaultAttributes.map((oneAttribute) => {
              return (
                <CartPageOneAttribute
                  key={oneAttribute.id}
                  oneAttribute={oneAttribute}
                  chosenAttributes={this.props.oneProduct.attributes}
                />
              );
            })}
          </div>
        </div>
        <div className="cartPageCounterImageWrapper">
          <div className="cartPageCounter">
            <div
              onClick={(e) =>
                this.increaseProductQuantity(
                  e,
                  this.props.oneProduct.id,
                  this.props.oneProduct.attributes
                )
              }>
              +
            </div>
            <p>{this.props.oneProduct.quantity}</p>
            <div
              onClick={(e) =>
                this.decreaseProductQuantity(
                  e,
                  this.props.oneProduct.id,
                  this.props.oneProduct.attributes
                )
              }>
              -
            </div>
          </div>
          <div className="cartPageImageWrapper">
            {this.state.showImage && <img src={this.state.showImage} />}
            {this.state.activeImage + 1 < this.state.imagesQuantity ? (
              <p
                className="cartPageImageRight"
                onClick={(e) => {
                  this.handleOnClickImages(e, 'increase');
                }}>
                {'>'}
              </p>
            ) : (
              ''
            )}
            {this.state.activeImage > 0 ? (
              <p
                className="cartPageImageLeft"
                onClick={(e) => {
                  this.handleOnClickImages(e, 'decrease');
                }}>
                {'<'}
              </p>
            ) : (
              ''
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default CartPageOneProduct;
