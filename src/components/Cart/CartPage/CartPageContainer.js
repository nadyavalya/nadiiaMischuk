import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import {
  increaseQuantity,
  decreaseQuantity,
  deleteProduct
} from '../../../redux/reducers/cartReducer';
import CartPage from './CartPage';

class CartPageContainer extends React.Component {
  render() {
    return (
      <CartPage
        chosenCurrency={this.props.currency}
        productsToBuy={this.props.productsToBuy}
        totalPrice={this.props.totalPrice}
        darkBackground={this.props.darkBackground}
        chosenCategory={this.props.chosenCategory}
        history={this.props.history}
        totalQuantityBuy={this.props.totalQuantityBuy}
        increaseQuantity={this.props.increaseQuantity}
        decreaseQuantity={this.props.decreaseQuantity}
        deleteProduct={this.props.deleteProduct}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  currency: state.currency.chosenCurrency,
  productsToBuy: state.cart.productsToBuy,
  totalPrice: state.cart.totalPrice,
  totalQuantityBuy: state.cart.totalQuantityBuy,
  darkBackground: state.app.darkBackground,
  chosenCategory: state.categories.chosenCategory
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    increaseQuantity,
    decreaseQuantity,
    deleteProduct
  })
)(CartPageContainer);
