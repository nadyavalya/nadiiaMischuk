import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch, withRouter } from 'react-router-dom';
import './App.css';
import HeaderContainer from './components/Header/HeaderContainer';
import { initializeApp } from './redux/reducers/appReducer';
import ProductsListContainer from './components/ProductsList/ProductsListContainer';
import ProductContainer from './components/ProductPage/ProductContainer';
import CartPageContainer from './components/Cart/CartPage/CartPageContainer';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.initializeApp();
  }

  render() {
    if (!this.props.initialized) {
      return <h2>Is loading...</h2>;
    }

    return (
      <BrowserRouter>
        <div>
          <HeaderContainer
            key={1}
            allCategories={this.props.allCategories}
            chosenCategory={this.props.chosenCategory}
          />
          <Switch>
            <Route path="/category/:categoryId" render={() => <ProductsListContainer />} />
            <Route path="/product/:productId" render={() => <ProductContainer />} />
            <Route path="/productsCart" render={() => <CartPageContainer />} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized,
  chosenCategory: state.categories.chosenCategory,
  allCategories: state.categories.allCategories
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    initializeApp
  })
)(App);
